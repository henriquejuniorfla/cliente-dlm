'use strict';

const request = require("request");
const fs = require('fs');
const Crypto = require('crypto');
const AES256 = require('aes256');
const NodeRSA = require('node-rsa');
const CHAVE_SISTEMA = "FLARTPMENGOLKJJ21396AKJ85DPOIUE8PULJaop789==";

//obter todas as obras cadastradas
exports.get = (req, res, next) => {
	
		res.status(200).send({
			response: "Oláaa",
			error: false
		});
	/*catch(function(error){
		res.status(400).send({
			response: "Houve um erro! "+error,
			error: true
		});
	});*/
}

exports.viewCliente = (req, res, next) => {
	res.render("view-cliente");
}

function decryptDocument(carteiraUsuario, hashTransacao, arquivoEncriptado, callback){
	try{
		request("http://127.0.0.1:8080/transacao/usuario/"+carteiraUsuario+"/obra/"+hashTransacao, function(error, response, body){
			var transacao = JSON.parse(body);
	        	//console.log(transacao);
	        if(transacao.response[0] != null && transacao.response[0] != "" && transacao.response[0] != undefined){
	            var chaveFinal = transacao.response[0].chave_final;
	            if(chaveFinal != null && chaveFinal != "" && chaveFinal != undefined){
	                //Decriptando a chave final com a chave do sistema (resultando em: chRSA)
	                var chRSA = AES256.decrypt(CHAVE_SISTEMA, chaveFinal);
	                //console.log(chRSA);
	                request("http://127.0.0.1:8080/usuario/"+carteiraUsuario+"/publicKey", function(error, response, body){
	                	var resposta = JSON.parse(body);
	                	var publicKey = resposta.response;
	                	if(publicKey != undefined && publicKey != null){
	                		var key = new NodeRSA({b: 512});
		                    key.importKey(publicKey, 'pkcs8-public-pem');
		                    var chaveAleatoria = key.decryptPublic(chRSA, 'utf8');

		                    //Arquivo decriptado com a chave do sistema
		                    var decryptedAES_sistema = AES256.decrypt(CHAVE_SISTEMA, arquivoEncriptado);
		                    //Arquivo decriptado com a chave aleatória
		                    var arquivoDecriptado = AES256.decrypt(chaveAleatoria, decryptedAES_sistema);
		                    
		                    //console.log(decryptedAES_sistema);
		                    //console.log(chaveAleatoria);
		                    var resposta = {
		                        status: true,
		                        message: arquivoDecriptado
		                    }
		                    callback(resposta);
		                }else{
		                	var resposta = {
			                    status: false,
			                    message: "Não foi possível encontrar a chave do usuário para decriptar o arquivo!"
			                }
			                callback(resposta);
		                }
	                });


	                //Decriptando a chave chRSA (decriptada anteriormente) com a chave pública do usuário 
	                /*var arquivoPublicKey = '/Users/Public/'+carteiraUsuario +'/pub-'+carteiraUsuario+'.pem';
	                if(fs.existsSync(arquivoPublicKey)){
		          		fs.readFile(arquivoPublicKey, function(err, publicKey){
		                    var key = new NodeRSA({b: 512});
		                    key.importKey(publicKey, 'pkcs8-public-pem');
		                    var chaveAleatoria = key.decryptPublic(chRSA, 'utf8');

		                    //Arquivo decriptado com a chave do sistema
		                    var decryptedAES_sistema = AES256.decrypt(CHAVE_SISTEMA, arquivoEncriptado);
		                    //Arquivo decriptado com a chave aleatória
		                    var arquivoDecriptado = AES256.decrypt(chaveAleatoria, decryptedAES_sistema);
		                    //console.log(decryptedAES_random);
		                    var resposta = {
		                        status: true,
		                        message: arquivoDecriptado
		                    }
		                    callback(resposta);
		                });
	                }else{
	                	var resposta = {
		                    status: false,
		                    message: "Não foi possível encontrar a chave do usuário para decriptar o arquivo!"
		                }
		                callback(resposta);
	                }*/
	            }else{
	                var resposta = {
	                    status: false,
	                    message: "Não foi possível decriptar o arquivo!"
	                }
	                callback(resposta);
	            }
	        }else{
	            var resposta = {
	                status: false,
	                message: "Arquivo não encontrado para decriptação!"
	            }
	            callback(resposta);
	        }           
	    });
	}catch(error){
		var resposta = {
	        status: false,
	        message: error
	    }
	    callback(resposta);
	}
}

exports.visualizarObra = (req, res, next) => {
	var carteiraUsuario = req.query.carteiraUsuario;
	var hashTransacao = req.query.hashTransacao;
	request("http://127.0.0.1:8080/usuario/"+carteiraUsuario+"/obra/"+hashTransacao, function(error, response, body){
		var result = JSON.parse(body);
		var conteudoObraEncriptado = result.response;

		if(result.error != true){
			decryptDocument(carteiraUsuario, hashTransacao, conteudoObraEncriptado, function(resposta){
				if(resposta.status == true){
					res.status(200).send({
						response: resposta.message
					});
				}else{
					res.status(400).send({
						response: resposta.message
					});
				}
			});
		}else{
			res.status(400).send({
				response: result.response
			});
		}
	});
}

exports.transferirObra = (req, res, next) => {
	var hashTransacao = req.query.hashTransacao;
	var carteiraUsuarioOrigem = req.query.carteiraUsuarioOrigem;
	var carteiraUsuarioDestino = req.query.carteiraUsuarioDestino;
	var validade = req.query.validade;

	if(validade == ""){
		validade = "null";
	}

	//console.log(req.query);
	request("http://127.0.0.1:8080/obra/"+hashTransacao+"/carteiraUsuarioOrigem/"+carteiraUsuarioOrigem+"/carteiraUsuarioDestino/"+carteiraUsuarioDestino+"/validade/"+validade, function(error, response, body){
		if(body != undefined && body != null){
			var result = JSON.parse(body);
			res.status(200).send({
				response: result.response,
				error: result.error
			});
		}else{
			res.status(400).send({
				response: "Não houve uma resposta do sistema DLM durante a transferência da obra",
				error: true
			});
		}
	});
}

exports.post = (req, res, next) => {
	
	res.status(200).send({
		method: "POST",
		body: req.body
	});
}

exports.put = (req, res, next) => {
	let id = req.params.id;
	res.status(200).send({
		method: "PUT",
		id: id,
		title: "Te amo Raiany!"
	});
}

exports.delete = (req, res, next) => {
	res.status(200).send({
		method: "DELETE",
		title: "Te amo Raiany!"
	});
}