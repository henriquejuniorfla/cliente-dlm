'use strict';

const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const cors = require('cors');
const app = express();

//Cors
app.use(cors());

//Carregar as Rotas
const indexRoute = require('./routes/index-route');
app.use("/", indexRoute);

//View
app.set('views', __dirname+'\\views');
app.set('view engine', 'ejs');    // Setamos que nossa engine será o ejs
app.use(expressLayouts);         // Definimos que vamos utilizar o express-ejs-layouts na nossa aplicação



module.exports = app;