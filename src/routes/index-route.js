'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/index-controller');

router.get('/', controller.get);

router.get('/view', controller.viewCliente);

router.get('/visualizarObra', controller.visualizarObra);

router.get('/transferirObra', controller.transferirObra);

router.post('/', controller.post);

router.put('/:id', controller.put);

router.delete('/', controller.delete);

module.exports = router;