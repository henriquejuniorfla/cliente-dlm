'use strict';

const http = require('http');
const app = require('../src/app');

const port = 8081;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);

console.log("Cliente DLM rodando na porta "+port);